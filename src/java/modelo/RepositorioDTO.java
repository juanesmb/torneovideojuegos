/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;


/**
 *
 * @author jefer
 */
public class RepositorioDTO {
    ArrayList<UsuarioDTO> usuarios;
     ArrayList<EventosDTO> eventos;
  
      public RepositorioDTO() {
       usuarios = new ArrayList<UsuarioDTO>(); 
       eventos = new ArrayList<EventosDTO>(); 
      }
    public RepositorioDTO(ArrayList<UsuarioDTO> usuarios, ArrayList<EventosDTO> eventos) {
        this.usuarios = usuarios;
        this.eventos = eventos;
    }


    public ArrayList<UsuarioDTO> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(ArrayList<UsuarioDTO> usuarios) {
        this.usuarios = usuarios;
    }

    public ArrayList<EventosDTO> getEventos() {
        return eventos;
    }

    public void setEventos(ArrayList<EventosDTO> eventos) {
        this.eventos = eventos;
    }

    @Override
    public String toString() {
        return "RepositorioDTO{" + "usuarios=" + usuarios + ", eventos=" + eventos + '}';
    }
     
     
}
