/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import static java.nio.file.Files.list;
import static java.util.Collections.list;
import java.util.List;

/**
 *
 * @author Estudiante
 */
public interface VideoManager {
    public void agregarVideo(VideoDTO video);
    public List<VideoDTO> mostrarVideo();
    public void borrarVideo(int id);
    public void actualizarVideo(int id);
}
