/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;
import java.util.List;
import baseDeDatos.MysqlDbAdapter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Estudiante
 */
public class VideoDAO implements VideoManager{

    List<VideoDTO> videos;
    
    @Override
    public void agregarVideo(VideoDTO video) {
        MysqlDbAdapter mysql = new MysqlDbAdapter();
        Connection connection = mysql.getConnection();
        try{
        PreparedStatement statement = connection.prepareStatement ("INSERT INTO video(id,url,nombre,idioma,categoria,descripcion) VALUES (NULL,?,?,?,?,?)");
        statement.setString(1, video.getUrl());
        statement.setString(2, video.getNombre());
        statement.setString(3, video.getIdioma());
        statement.setString(4, video.getCategoria());
        statement.setString(5, video.getDescripcion());
        statement.executeUpdate();
        connection.close();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    

    @Override
    public List<VideoDTO> mostrarVideo() {
        List<VideoDTO> videos=new LinkedList();
        
        MysqlDbAdapter mysql=new MysqlDbAdapter();
        Connection connection = mysql.getConnection();
        try{
        String sql="SELECT * FROM video";
        Statement sentencia= connection.prepareStatement(sql);
        sentencia.executeQuery(sql);
        ResultSet resultado = sentencia.executeQuery(sql);
        while(resultado.next()){
         videos.add(new VideoDTO(resultado.getInt("id"),resultado.getString(2),resultado.getString(3),resultado.getString(4),resultado.getString(5),resultado.getString(6)));
        }
        return videos;
        }
        catch(Exception e){
            e.printStackTrace();
         return null;
        }
        
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void borrarVideo(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actualizarVideo(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

  
    
}

