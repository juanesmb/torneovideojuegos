/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Estudiante
 */

public class VideoDTO {
    private int id;
    private String url;
    private String nombre;
    private String idioma;
    private String categoria;
    private String descripcion;
    
     public VideoDTO() {
    } 
    
    public VideoDTO(int id, String url, String nombre, String idioma, String categoria, String descripcion) {
        this.id = id;
        this.url = url;
        this.nombre = nombre;
        this.idioma = idioma;
        this.categoria = categoria;
        this.descripcion = descripcion;
    }
 

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "VideoDTO{" + "id=" + id + ", url=" + url + ", nombre=" + nombre + ", idioma=" + idioma + ", categoria=" + categoria + ", descripcion=" + descripcion + '}';
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    
}
