package modelo;

public class VideoJuegoDTO {
    private String nombre;
    private String descripcion;
    private short calificacion;
    private String genero;
    private String estado;
    
    public VideoJuegoDTO() {
        this.nombre = "";
        this.descripcion = "";
        this.calificacion = 0;
        this.genero = "";
        this.estado = "";
    }

    public VideoJuegoDTO(String nombre, String descripcion, short calificacion, String genero, String estado) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.calificacion = calificacion;
        this.genero = genero;
        this.estado = estado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public short getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(short calificacion) {
        this.calificacion = calificacion;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "VideoJuegoDTO{" + "nombre=" + nombre + ", descripcion=" + descripcion + ", calificacion=" + calificacion + ", genero=" + genero + ", estado=" + estado + '}';
    }

    
    
    
}
