package modelo;

public class UsuarioDTO {
    private String nombres;
    private String apellidos;
    private String correo;
    private String contraseña;
    private VideoJuegoDTO videojuego;
    
    public UsuarioDTO() {
        this.nombres = "";
        this.apellidos = "";
        this.correo = "";
        this.contraseña = "";
        this.videojuego = null;
    }

    public UsuarioDTO(String nombres, String apellidos, String correo, String contraseña,VideoJuegoDTO videojuego) {
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.correo = correo;
        this.contraseña = contraseña;
        this.videojuego = videojuego;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    @Override
    public String toString() {
        return "UsuarioDTO{" + "nombres=" + nombres + ", apellidos=" + apellidos + ", correo=" + correo + ", contraseña=" + contraseña + '}';
    }
    
    
    
}
